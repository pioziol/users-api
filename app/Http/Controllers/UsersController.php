<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MyApp\Application\AddUser\AddUser;
use MyApp\Application\AddUser\AddUserDefaultRequest as AddUserRequest;
use MyApp\Application\Boundary\PresenterFactory;
use MyApp\Application\DeleteUser\DeleteUser;
use MyApp\Application\DeleteUser\DeleteUserDefaultRequest;
use MyApp\Application\GetUser\GetUser;
use MyApp\Application\GetUser\GetUserDefaultRequest;
use MyApp\Application\ListUsers\ListUsers;
use MyApp\Application\ListUsers\ListUsersDefaultRequest;
use MyApp\Application\UpdateUser\UpdateUser;
use MyApp\Application\UpdateUser\UpdateUserDefaultRequest;
use MyApp\Domain\Mapper\DomainFields;

class UsersController extends Controller
{
    /** @var PresenterFactory */
    private $presenterFactory;

    public function __construct(PresenterFactory $presenterFactory)
    {
        $this->presenterFactory = $presenterFactory;
    }

    public function index(
        $format,
        Request $request,
        ListUsers $listUsers
    )
    {
        $request = new ListUsersDefaultRequest($request->all());
        $presenter = $this->presenterFactory->create(ListUsers::class, $format);
        $listUsers->execute($request, $presenter);
        return $presenter->view();
    }

    public function show(
        $format,$id,
        GetUser $getUser
    )
    {
        $request = new GetUserDefaultRequest([DomainFields::USER_ID_FIELD => $id]);
        $presenter = $this->presenterFactory->create(GetUser::class, $format);
        $getUser->execute($request, $presenter);
        return $presenter->view();
    }

    public function create(
        $format,
        Request $request,
        AddUser $addUser
    )
    {
        $request = new AddUserRequest($request->only([
            DomainFields::EMAIL_FIELD,
            DomainFields::PASSWORD_FIELD,
            DomainFields::REPEATED_PASSWORD_FIELD,
            DomainFields::NAME_FIELD,
            DomainFields::LAST_NAME_FIELD
        ]));
        $presenter = $this->presenterFactory->create(AddUser::class, $format);
        $addUser->execute($request, $presenter);
        return $presenter->view();
    }

    public function update(
        $format,
        Request $request,
        UpdateUser $updateUser
    )
    {
        $request = new UpdateUserDefaultRequest($request->only([
            DomainFields::LAST_NAME_FIELD,
            DomainFields::NAME_FIELD,
            DomainFields::PASSWORD_FIELD,
            DomainFields::USER_ID_FIELD
        ]));
        $presenter = $this->presenterFactory->create(UpdateUser::class, $format);
        $updateUser->execute($request, $presenter);
        return $presenter->view();
    }

    public function delete(
        Request $request,
        $format,
        DeleteUser $deleteUser
    )
    {
        $request = new DeleteUserDefaultRequest(
            $request->get(DomainFields::USER_ID_FIELD),
            $request->get(DomainFields::PASSWORD_FIELD)
        );
        $presenter = $this->presenterFactory->create(DeleteUser::class, $format);
        $deleteUser->execute($request, $presenter);
        return $presenter->view();
    }
}
