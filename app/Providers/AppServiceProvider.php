<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use MyApp\Domain\Gateway\UserGateway;
use MyApp\Infrastructure\Gateway\UserModelGateway;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserGateway::class, UserModelGateway::class);
    }
}
