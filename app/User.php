<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'password', 'last_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function getId():int
    {
        return $this->getAttribute('id');
    }

    public function getEmail():string
    {
        return $this->getAttribute('email');
    }

    public function getName():string
    {
        return $this->getAttribute('first_name');
    }

    public function getLastName():string
    {
        return $this->getAttribute('last_name');
    }

    public function getPassword()
    {
        return $this->getAttribute('password');
    }
}
