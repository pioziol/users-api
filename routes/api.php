<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('{format}/users', 'UsersController@index');
Route::get('{format}/user/{id}', 'UsersController@show');
Route::put('/user', 'UsersController@create');
Route::post('{format}/user', 'UsersController@update');
Route::delete('{format}/user', 'UsersController@delete');