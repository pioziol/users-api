<?php

namespace MyApp\Application\AddUser;

use MyApp\Domain\Entity\User;
use MyApp\Domain\Factory\UserFactory;
use MyApp\Domain\Gateway\UserGateway;

class AddUser
{
    /** @var UserFactory */
    private $userFactory;

    private $userGateway;

    public function __construct(
        UserFactory $userFactory,
        UserGateway $userGateway
    )
    {
        $this->userFactory = $userFactory;
        $this->userGateway = $userGateway;
    }

    public function execute(AddUserInputBoundary $request, AddUserOutputBoundary $presenter)
    {
        $response = new AddUserResponse();
        if (!(new AddUserValidator())->validate($request, $response)) {
            $presenter->present($response);
            return;
        }
        try {
            /** @var User $user */
            $user = $this->userFactory->create($request->toArray());
            $this->userGateway->save($user);
            $response->setResult($user);
        } catch (\Exception $ex) {
            echo $ex->getMessage();exit;
        }
        $presenter->present($response);
    }

}