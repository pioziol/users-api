<?php

namespace MyApp\Application\AddUser;

use MyApp\Domain\Mapper\DomainFields;

class AddUserDefaultRequest implements AddUserInputBoundary
{
    private $email;
    private $password;
    private $name;
    private $lastName;
    private $repeatedPassword;

    public function __construct($data)
    {
        $this->email = $data[DomainFields::EMAIL_FIELD];
        $this->password = $data[DomainFields::PASSWORD_FIELD];
        $this->name = $data[DomainFields::NAME_FIELD];
        $this->lastName = $data[DomainFields::LAST_NAME_FIELD];
        $this->repeatedPassword = $data[DomainFields::REPEATED_PASSWORD_FIELD];
    }




    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    public function getRepeatedPassword()
    {
        return $this->repeatedPassword;
    }


    public function toArray()
    {
        return [
            DomainFields::EMAIL_FIELD => $this->email,
            DomainFields::PASSWORD_FIELD => $this->password,
            DomainFields::NAME_FIELD => $this->name,
            DomainFields::LAST_NAME_FIELD => $this->lastName,
            DomainFields::REPEATED_PASSWORD_FIELD =>$this->repeatedPassword
        ];
    }


}