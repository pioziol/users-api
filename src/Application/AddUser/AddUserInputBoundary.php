<?php

namespace MyApp\Application\AddUser;

interface AddUserInputBoundary
{
    public function getEmail();

    public function getPassword();

    public function getName();

    public function getLastName();

    public function getRepeatedPassword();

    public function toArray();
}