<?php

namespace MyApp\Application\AddUser;

interface AddUserOutputBoundary
{
    public function present(AddUserResponse $response);

    public function view();

}