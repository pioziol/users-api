<?php

namespace MyApp\Application\AddUser;


use MyApp\Domain\Entity\User;

class AddUserResponse
{
    /** @var  User */
    private $result;
    private $success;
    private $errors = [];

    public function setResult(User $user)
    {
        $this->result = $user;
        $this->success = true;
    }

    public function setError($error)
    {
        $this->errors = array_merge($this->errors, (array)$error);
        $this->success = false;
    }

    public function isSuccess():bool{
        return $this->success;
    }

    public function getErrors():array{
        return $this->errors;
    }

    public function getResult():User
    {
        return $this->result;
    }

}