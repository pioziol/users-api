<?php

namespace MyApp\Application\AddUser;

use MyApp\Domain\Exception\ValidationException;
use MyApp\Domain\ValueObject\EmailVO;
use MyApp\Domain\ValueObject\PasswordVO;

class AddUserValidator
{
    public function validate(AddUserInputBoundary $request, AddUserResponse $response)
    {
        $errors = [];

        try {
            (new PasswordVO($request->getPassword()))
                ->sameAs(new PasswordVO($request->getRepeatedPassword()));
        } catch (ValidationException $ex) {
            array_push($errors, $ex->getMessage());
        }

        try {
            new EmailVO($request->getEmail());
        } catch (ValidationException $ex) {
            array_push($errors, $ex->getMessage());
        }

        if (empty($errors)) return true;
        $response->setError($errors);
        return false;
    }

}