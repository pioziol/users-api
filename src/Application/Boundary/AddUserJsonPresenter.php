<?php

namespace MyApp\Application\Boundary;


use MyApp\Application\AddUser\AddUserOutputBoundary;
use MyApp\Application\AddUser\AddUserResponse;
use MyApp\Domain\Entity\User;
use MyApp\Domain\Mapper\DomainFields;

class AddUserJsonPresenter implements AddUserOutputBoundary
{
    /** @var  AddUserResponse */
    private $result;

    public function present(AddUserResponse $result)
    {
        $this->result = $result;
    }

    public function view()
    {
        if ($this->result->isSuccess()) {
            /** @var User $user */
            $user = $this->result->getResult();
            return response()->json([
                DomainFields::USER_ID_FIELD => $user->getId()
            ]);
        }
        return response()->json(
            ['errors' => $this->result->getErrors()]
        );
    }


}