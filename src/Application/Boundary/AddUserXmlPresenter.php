<?php

namespace MyApp\Application\Boundary;


use MyApp\Application\AddUser\AddUserOutputBoundary;
use MyApp\Application\AddUser\AddUserResponse;
use MyApp\Domain\Mapper\DomainFields;

class AddUserXmlPresenter implements AddUserOutputBoundary
{

    /** @var AddUserResponse */
    private $response;

    public function present(AddUserResponse $response)
    {
        $this->response = $response;
    }

    public function view()
    {
        $responseXML =new \SimpleXMLElement('<response />');
        if ($this->response->isSuccess()) {
            $user = $this->response->getResult();
            $responseXML->addChild(DomainFields::USER_ID_FIELD, $user->getId());
        } else {
            $errors = $this->response->getErrors();
            foreach($errors as $error) $responseXML->addChild('error',$error);
        }
        return  response($responseXML->saveXML())->header('Content-Type', 'application/xml');
    }

}