<?php

namespace MyApp\Application\Boundary;

use MyApp\Application\DeleteUser\DeleteUserOutputBoundary;
use MyApp\Application\DeleteUser\DeleteUserResponse;

class DeleteUserJsonPresenter implements DeleteUserOutputBoundary
{
    /** @var  DeleteUserResponse */
    private $result;

    public function present(DeleteUserResponse $result)
    {
        $this->result = $result;
    }

    public function view()
    {
        if ($this->result->isSuccess()) {
            return response()->json('Success');
        }
        return response()->json(
            ['errors' => $this->result->getErrors()]
        );
    }

}