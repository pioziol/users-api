<?php

namespace MyApp\Application\Boundary;

use MyApp\Application\DeleteUser\DeleteUserOutputBoundary;
use MyApp\Application\DeleteUser\DeleteUserResponse;

class DeleteUserXmlPresenter implements DeleteUserOutputBoundary
{
    /** @var DeleteUserResponse */
    private $result;

    public function present(DeleteUserResponse $result)
    {
        $this->result = $result;
    }

    public function view()
    {
        $responseXML =new \SimpleXMLElement('<response />');
        if ($this->result->isSuccess()) {
            $responseXML->addChild('Success','true');
        } else {
            $errors = $this->result->getErrors();
            foreach($errors as $error) $responseXML->addChild('error',$error);
        }
        return  response($responseXML->saveXML())->header('Content-Type', 'application/xml');
    }
}