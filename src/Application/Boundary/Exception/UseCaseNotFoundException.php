<?php

namespace MyApp\Application\Boundary\Exception;

class UseCaseNotFoundException extends \Exception
{
    public function __construct($useCase)
    {
        parent::__construct("Use case {$useCase} cannot be found!");
    }

}