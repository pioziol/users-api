<?php

namespace MyApp\Application\Boundary\Exception;

class WrongPresentationFormatException extends \Exception
{
    public function __construct($format)
    {
        parent::__construct("Presentation format {$format} doesn't exist!");
    }

}