<?php

namespace MyApp\Application\Boundary;

use MyApp\Application\GetUser\GetUserOutputBoundary;
use MyApp\Application\GetUser\GetUserResponse;
use MyApp\Domain\Entity\User;
use MyApp\Domain\Mapper\DomainFields;

class GetUserJsonPresenter implements GetUserOutputBoundary
{
    /** @var  GetUserResponse */
    private $result;

    public function present(GetUserResponse $result)
    {
        $this->result = $result;
    }

    public function view()
    {
        if ($this->result->isSuccess()) {
            /** @var User $user */
            $user = $this->result->getResult();
            return response()->json([
                DomainFields::USER_ID_FIELD => $user->getId(),
                DomainFields::NAME_FIELD => $user->getName(),
                DomainFields::LAST_NAME_FIELD => $user->getLastName(),
                DomainFields::EMAIL_FIELD => $user->getEmail()
            ]);
        }
        return response()->json(
            ['errors' => $this->result->getErrors()]
        );
    }

}