<?php

namespace MyApp\Application\Boundary;

use MyApp\Application\GetUser\GetUserOutputBoundary;
use MyApp\Application\GetUser\GetUserResponse;
use MyApp\Domain\Mapper\DomainFields;

class GetUserXmlPresenter implements GetUserOutputBoundary
{
    /** @var GetUserResponse */
    private $response;

    public function present(GetUserResponse $response)
    {
        $this->response = $response;
    }

    public function view()
    {
        $responseXML = new \SimpleXMLElement('<response />');
        if ($this->response->isSuccess()) {
            $user = $this->response->getResult();
            $responseXML->addChild(DomainFields::USER_ID_FIELD, $user->getId());
            $responseXML->addChild(DomainFields::LAST_NAME_FIELD, $user->getLastName());
            $responseXML->addChild(DomainFields::NAME_FIELD, $user->getName());
            $responseXML->addChild(DomainFields::EMAIL_FIELD, $user->getEmail());
        } else {
            $errors = $this->response->getErrors();
            foreach ($errors as $error) $responseXML->addChild('error', $error);
        }
        return response($responseXML->saveXML())->header('Content-Type', 'application/xml');
    }

}