<?php

namespace MyApp\Application\Boundary;

use MyApp\Application\ListUsers\ListUsersOutputBoundary;
use MyApp\Application\ListUsers\ListUsersResponse;
use MyApp\Domain\Entity\User;
use MyApp\Domain\Mapper\DomainFields;

class ListUsersJsonPresenter implements ListUsersOutputBoundary
{
    /** @var ListUsersResponse */
    private $result;

    public function present(ListUsersResponse $result)
    {
        $this->result = $result;
    }

    public function view()
    {
        if ($this->result->isSuccess()) {
            $users = $this->result->getResult();
            $usersArray = [];
            /** @var User $user */
            foreach ($users as $user) {
                $userArray = [
                    DomainFields::USER_ID_FIELD => $user->getId(),
                    DomainFields::LAST_NAME_FIELD => $user->getLastName(),
                    DomainFields::EMAIL_FIELD => $user->getEmail(),
                    DomainFields::NAME_FIELD => $user->getName()
                ];
                array_push($usersArray, $userArray);
            }
            return response()->json($usersArray);
        }
        return response()->json(
            ['errors' => $this->result->getErrors()]
        );
    }

}