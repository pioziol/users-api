<?php

namespace MyApp\Application\Boundary;


use MyApp\Application\ListUsers\ListUsersOutputBoundary;
use MyApp\Application\ListUsers\ListUsersResponse;
use MyApp\Domain\Entity\User;
use MyApp\Domain\Mapper\DomainFields;
use Sabre\Xml\Service;

class ListUsersXmlPresenter implements ListUsersOutputBoundary
{
    /** @var ListUsersResponse */
    private $response;

    public function present(ListUsersResponse $response)
    {
        $this->response = $response;
    }

    public function view()
    {

        if ($this->response->isSuccess()) {
            $writer = new Service();
            $users = $this->response->getResult();
            $usersArray = [];
            /** @var User $user */
            foreach ($users as $user) {
                $userArray = [
                    DomainFields::USER_ID_FIELD => $user->getId(),
                    DomainFields::LAST_NAME_FIELD => $user->getLastName(),
                    DomainFields::EMAIL_FIELD => $user->getEmail(),
                    DomainFields::NAME_FIELD => $user->getName()
                ];
                array_push($usersArray, ['user' => $userArray]);

            }
            return $writer->write('response', $usersArray);
        }
        $responseXML = new \SimpleXMLElement('<response />');
        $errors = $this->response->getErrors();
        foreach ($errors as $error) $responseXML->addChild('error', $error);
        return response($responseXML->saveXML())->header('Content-Type', 'application/xml');
    }


}