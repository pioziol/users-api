<?php

namespace MyApp\Application\Boundary;

use MyApp\Application\AddUser\AddUser;
use MyApp\Application\Boundary\Exception\UseCaseNotFoundException;
use MyApp\Application\Boundary\Exception\WrongPresentationFormatException;
use MyApp\Application\DeleteUser\DeleteUser;
use MyApp\Application\GetUser\GetUser;
use MyApp\Application\ListUsers\ListUsers;
use MyApp\Application\UpdateUser\UpdateUser;

class PresenterFactory
{
    public function create($useCase, $type)
    {
        switch ($useCase) {
            case AddUser::class:
                if ($type === 'xml') return new AddUserXmlPresenter();
                if($type === 'json') return new AddUserJsonPresenter();
                break;
            case UpdateUser::class:
                if ($type === 'xml') return new UpdateUserXmlPresenter();
                if($type === 'json') return new UpdateUserJsonPresenter();
                break;
            case GetUser::class:
                if ($type === 'xml') return new GetUserXmlPresenter();
                if($type === 'json') return new GetUserJsonPresenter();
                break;
            case ListUsers::class:
                if ($type === 'xml') return new ListUsersXmlPresenter();
                if($type === 'json') return new ListUsersJsonPresenter();
                break;
            case DeleteUser::class:
                if($type === 'xml') return new DeleteUserXmlPresenter();
                if($type === 'json') return new DeleteUserJsonPresenter();
                break;
            default:
                throw new UseCaseNotFoundException($useCase);
        }
        throw new WrongPresentationFormatException($type);
    }

}