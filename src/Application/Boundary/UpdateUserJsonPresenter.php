<?php

namespace MyApp\Application\Boundary;


use MyApp\Application\UpdateUser\UpdateUserOutputBoundary;
use MyApp\Application\UpdateUser\UpdateUserResponse;
use MyApp\Domain\Entity\User;
use MyApp\Domain\Mapper\DomainFields;

class UpdateUserJsonPresenter implements UpdateUserOutputBoundary
{
    /** @var  UpdateUserResponse */
    private $result;
    public function present(UpdateUserResponse $response)
    {
        $this->result = $response;
    }

    public function view()
    {
        if ($this->result->isSuccess()) {
            /** @var User $user */
            $user = $this->result->getResult();
            return response()->json([
                DomainFields::USER_ID_FIELD => $user->getId(),
                DomainFields::NAME_FIELD => $user->getName(),
                DomainFields::LAST_NAME_FIELD => $user->getLastName(),
                DomainFields::EMAIL_FIELD => $user->getEmail()
            ]);
        }
        return response()->json(
            ['errors' => $this->result->getErrors()]
        );
    }


}