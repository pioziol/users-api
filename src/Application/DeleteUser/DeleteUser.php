<?php

namespace MyApp\Application\DeleteUser;


use MyApp\Domain\Command\CheckUserPassword\CheckUserPassword;
use MyApp\Domain\Command\CommandExecutor;
use MyApp\Domain\Gateway\UserGateway;
use MyApp\Domain\Mapper\DomainFields;

class DeleteUser
{
    /** @var UserGateway */
    private $userGateway;

    /** @var CommandExecutor */
    private $commandExecutor;

    public function __construct(
        UserGateway $userGateway,
        CommandExecutor $commandExecutor
    )
    {
        $this->userGateway = $userGateway;
        $this->commandExecutor = $commandExecutor;
    }

    public function execute(DeleteUserInputBoundary $request, DeleteUserOutputBoundary $presenter)
    {
        $response = new DeleteUserResponse();
        if (!(new DeleteUserValidator())->validate($request, $response)) {
            $presenter->present($response);
            return;
        }
        try {
            $this->commandExecutor->execute(new CheckUserPassword([
                DomainFields::USER_ID_FIELD => $request->getUserId(),
                DomainFields::PASSWORD_FIELD => $request->getPassword()
            ]));
            $this->userGateway->delete($request->getUserId());
            $response->setResponse('Success!');
        }catch (\Exception $ex) {
            $response->setError($ex->getMessage());
        }
        $presenter->present($response);
    }

}