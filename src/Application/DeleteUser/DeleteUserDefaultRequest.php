<?php

namespace MyApp\Application\DeleteUser;

class DeleteUserDefaultRequest implements DeleteUserInputBoundary
{
    private $userId;
    private $password;

    public function __construct($userId, $password)
    {
        $this->userId = $userId;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    public function getPassword()
    {
        return $this->password;
    }
}