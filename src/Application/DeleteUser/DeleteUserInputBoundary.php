<?php

namespace MyApp\Application\DeleteUser;

interface DeleteUserInputBoundary
{
    public function getUserId();

    public function getPassword();

}