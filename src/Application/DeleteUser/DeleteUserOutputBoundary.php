<?php

namespace MyApp\Application\DeleteUser;

interface DeleteUserOutputBoundary
{
    public function present(DeleteUserResponse $result);
    public function view();

}