<?php

namespace MyApp\Application\DeleteUser;

class DeleteUserResponse
{
    private $response;
    private $errors = [];
    private $success;

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
        $this->success = true;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param $errors
     */
    public function setError($errors)
    {
        $this->errors = array_merge($this->errors, (array)$errors);
        $this->success = false;
    }

    public function isSuccess()
    {
        return $this->success;
    }



}