<?php

namespace MyApp\Application\DeleteUser;


class DeleteUserValidator
{
    public function validate(DeleteUserInputBoundary $request, DeleteUserResponse $response)
    {
        $errors = [];

        if(empty($request->getUserId())) {
            array_push($errors, 'Invalid user id!');
        }

        if (empty($errors)) return true;
        $response->setError($errors);
        return false;
    }
}