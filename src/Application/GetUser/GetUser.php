<?php

namespace MyApp\Application\GetUser;

use MyApp\Domain\Gateway\UserGateway;

class GetUser
{
    /** @var UserGateway */
    private $userGateway;

    public function __construct(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    public function execute(GetUserInputBoundary $request, GetUserOutputBoundary $presenter)
    {
        $response = new GetUserResponse();
        if (!(new GetUserValidator())->validate($request, $response)) {
            $presenter->present($response);
            return;
        }

        try {
            $user = $this->userGateway->find($request->getId());
            $response->setResponse($user);
        } catch (\Exception $ex) {
            $response->setError($ex->getMessage());
        }
        $presenter->present($response);
    }

}