<?php

namespace MyApp\Application\GetUser;


use MyApp\Domain\Mapper\DomainFields;

class GetUserDefaultRequest implements GetUserInputBoundary
{
    private $id;

    public function __construct($data)
    {
        $this->id = $data[DomainFields::USER_ID_FIELD];
    }

    public function getId()
    {
        return $this->id;
    }
}