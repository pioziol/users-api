<?php

namespace MyApp\Application\GetUser;

interface GetUserInputBoundary
{
    public function getId();
}