<?php

namespace MyApp\Application\GetUser;

interface GetUserOutputBoundary
{
    public function present(GetUserResponse $response);

    public function view();

}