<?php

namespace MyApp\Application\GetUser;


use MyApp\Domain\Entity\User;

class GetUserResponse
{

    private $result;
    private $success;
    private $errors = [];

    public function setResponse(User $result)
    {
        $this->result = $result;
        $this->success = true;
    }

    public function setError($error)
    {
        $this->errors = array_merge($this->errors, (array)$error);
        $this->success = false;
    }

    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @return User
     */
    public function getResult():User
    {
        return $this->result;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}