<?php

namespace MyApp\Application\GetUser;

class GetUserValidator
{
    public function validate(GetUserInputBoundary $request, GetUserResponse $response)
    {
        $errors = [];

        if (empty($request->getId()) || !is_numeric($request->getId())) {
            array_push($errors, 'Invalid user id');
        }

        if (empty($errors)) return true;
        $response->setError($errors);
        return false;
    }
}