<?php

namespace MyApp\Application\ListUsers;

use MyApp\Domain\Repository\UserRepository;

class ListUsers
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(ListUsersInputBoundary $request, ListUsersOutputBoundary $presenter)
    {
        $response = new ListUsersResponse();
        try {
            $list = $this->userRepository->findManyWithFilters($request->toArray());
            $response->setResult($list);
        } catch (\Exception $ex) {
            $response->setError($ex->getMessage());
        }
        $presenter->present($response);

    }

}