<?php

namespace MyApp\Application\ListUsers;

use MyApp\Domain\Mapper\DomainFields;

class ListUsersDefaultRequest implements ListUsersInputBoundary
{
    private $id;
    private $name;
    private $lastName;
    private $email;
    private $offset;
    private $limit;
    private $orderBy;

    public function __construct(array $data)
    {
        $this->id = $data[DomainFields::USER_ID_FIELD]??null;
        $this->name = $data[DomainFields::NAME_FIELD]??null;
        $this->lastName = $data[DomainFields::LAST_NAME_FIELD]??null;
        $this->email = $data[DomainFields::EMAIL_FIELD]??null;
        $this->offset = $data['offset']??null;
        $this->limit = $data['limit']??null;
        $this->orderBy = $data['orderBy']??null;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }


    public function toArray()
    {
        return [
            DomainFields::USER_ID_FIELD => $this->id,
            DomainFields::NAME_FIELD => $this->name,
            DomainFields::LAST_NAME_FIELD => $this->lastName,
            DomainFields::EMAIL_FIELD => $this->email,
            'limit' => $this->email,
            'offset' => $this->offset,
            'orderBy' => $this->orderBy,
        ];
    }
}