<?php

namespace MyApp\Application\ListUsers;

interface ListUsersInputBoundary
{

    public function getId();

    public function getName();

    public function getLastName();

    public function getEmail();

    public function toArray();

}