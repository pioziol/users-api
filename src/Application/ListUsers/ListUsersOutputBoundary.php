<?php

namespace MyApp\Application\ListUsers;

interface ListUsersOutputBoundary
{
    public function present(ListUsersResponse $response);

    public function view();

}