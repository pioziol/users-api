<?php

namespace MyApp\Application\ListUsers;

class ListUsersResponse
{
    private $result;
    private $errors = [];
    private $success;


    public function setResult($result)
    {
        $this->result = $result;
        $this->success = true;
    }

    public function setError($error)
    {
        $this->errors = array_merge($this->errors, (array)$error);
        $this->success = false;
    }

    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }





}