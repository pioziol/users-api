<?php

namespace MyApp\Application\UpdateUser;

use MyApp\Domain\Command\CheckUserPassword\CheckUserPassword;
use MyApp\Domain\Command\CommandExecutor;
use MyApp\Domain\Factory\UserFactory;
use MyApp\Domain\Mapper\DomainFields;
use MyApp\Domain\Repository\UserRepository;

class UpdateUser
{
    private $commandExecutor;

    private $userRepository;

    private $userFactory;

    public function __construct(
        CommandExecutor $commandExecutor,
        UserRepository $userRepository,
        UserFactory $userFactory
    )
    {
        $this->commandExecutor = $commandExecutor;
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
    }

    public function execute(UpdateUserInputBoundary $request, UpdateUserOutputBoundary $presenter)
    {
        $response = new UpdateUserResponse();
        if (!(new UpdateUserValidator())->validate($request, $response)) {
            $presenter->present($response);
            return;
        }

        try {
            $this->commandExecutor->execute(new CheckUserPassword([
                DomainFields::PASSWORD_FIELD => $request->getPassword(),
                DomainFields::USER_ID_FIELD => $request->getUserId()
            ]));
            $user = $this->userFactory->create($request->toArray());

            $user = $this->userRepository->update($user);
            $response->setResult($user);
        } catch(\Exception $ex) {
            $response->setError($ex->getMessage());
        }
        $presenter->present($response);
    }
}