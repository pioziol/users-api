<?php

namespace MyApp\Application\UpdateUser;


use MyApp\Domain\Mapper\DomainFields;

class UpdateUserDefaultRequest implements UpdateUserInputBoundary
{
    private $userId;
    private $name;
    private $lastName;
    private $password;

    public function __construct(array $data)
    {
        $this->userId = $data[DomainFields::USER_ID_FIELD];
        $this->password = $data[DomainFields::PASSWORD_FIELD];
        $this->name = $data[DomainFields::NAME_FIELD];
        $this->lastName = $data[DomainFields::LAST_NAME_FIELD];
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function toArray(): array
    {
        return [
            DomainFields::USER_ID_FIELD => $this->userId,
            DomainFields::PASSWORD_FIELD => $this->password,
            DomainFields::NAME_FIELD => $this->name,
            DomainFields::LAST_NAME_FIELD => $this->lastName
        ];
    }


}