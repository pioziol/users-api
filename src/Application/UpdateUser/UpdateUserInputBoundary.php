<?php

namespace MyApp\Application\UpdateUser;

interface UpdateUserInputBoundary
{
    public function getLastName();
    public function getName();
    public function getPassword();
    public function getUserId();
    public function toArray():array;

}