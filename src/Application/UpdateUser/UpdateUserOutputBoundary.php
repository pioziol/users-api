<?php

namespace MyApp\Application\UpdateUser;

interface UpdateUserOutputBoundary
{

    public function present(UpdateUserResponse $response);

    public function view();

}