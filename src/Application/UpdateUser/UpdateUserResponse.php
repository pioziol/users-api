<?php

namespace MyApp\Application\UpdateUser;

use MyApp\Domain\Entity\User;

class UpdateUserResponse
{
    private $result;
    private $success;
    private $errors = [];

    public function setError($error)
    {
        $this->errors = array_merge($this->errors, (array)$error);
        $this->success = false;
    }

    public function setResult(User $result)
    {
        $this->result = $result;
        $this->success = true;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getResult():User
    {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getErrors():array
    {
        return $this->errors;
    }



}