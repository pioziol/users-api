<?php

namespace MyApp\Application\UpdateUser;


use MyApp\Domain\Exception\ValidationException;
use MyApp\Domain\ValueObject\PasswordVO;

class UpdateUserValidator
{
    public function validate(UpdateUserInputBoundary $request, UpdateUserResponse $response)
    {
        $errors = [];

        try {
            new PasswordVO($request->getPassword());
        } catch (ValidationException $ex) {
            array_push($errors, $ex->getMessage());
        }

        if (empty($request->getUserId())) {
            array_push($errors, $ex->getMessage());
        }

        if(empty($errors)) return true;
        $response->setError($errors);
        return false;
    }

}