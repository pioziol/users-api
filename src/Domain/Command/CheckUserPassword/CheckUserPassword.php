<?php

namespace MyApp\Domain\Command\CheckUserPassword;

use MyApp\Domain\Command\Command;
use MyApp\Domain\Mapper\DomainFields;

class CheckUserPassword implements Command
{
    private $userId;
    private $password;

    public function __construct(array $data)
    {
        $this->userId = $data[DomainFields::USER_ID_FIELD];
        $this->password = $data[DomainFields::PASSWORD_FIELD];
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }
}