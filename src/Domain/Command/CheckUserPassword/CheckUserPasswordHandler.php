<?php

namespace MyApp\Domain\Command\CheckUserPassword;

use MyApp\Domain\Exception\InvalidPasswordException;
use MyApp\Domain\Gateway\UserGateway;
use MyApp\Domain\Service\HashPassword;

class CheckUserPasswordHandler
{
    /** @var UserGateway */
    private $userGateway;

    /** @var HashPassword */
    private $hashPassword;

    public function __construct(
        UserGateway $userGateway,
        HashPassword $hashPassword
    )
    {
        $this->userGateway = $userGateway;
        $this->hashPassword = $hashPassword;
    }

    public function execute(CheckUserPassword $command)
    {
        $password = $this->userGateway->getPasswordForId($command->getUserId());
        if ($password !== $this->hashPassword->execute($command->getPassword())) throw new InvalidPasswordException();
    }

}