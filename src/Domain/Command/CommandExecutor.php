<?php

namespace MyApp\Domain\Command;


use MyApp\Domain\Command\CheckUserPassword\CheckUserPassword;
use MyApp\Domain\Command\CheckUserPassword\CheckUserPasswordHandler;
use MyApp\Domain\Exception\CommandNotFoundException;

class CommandExecutor
{
    public function __construct(
        CheckUserPasswordHandler $checkUserPassword
    )
    {
        $this->checkUserPassword = $checkUserPassword;
    }

    public function execute(Command $command)
    {
        $commandName = (new \ReflectionClass($command))->getName();
        switch ($commandName) {
            case CheckUserPassword::class:
                $this->checkUserPassword->execute($command);
                break;
            default:
                throw new CommandNotFoundException($commandName);
        }
    }

}