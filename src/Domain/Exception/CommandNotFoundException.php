<?php

namespace MyApp\Domain\Exception;

use Throwable;

class CommandNotFoundException extends \Exception
{
    public function __construct($commandName = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct("Command {$commandName} cannot be found", $code, $previous);
    }

}