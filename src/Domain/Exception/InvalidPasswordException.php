<?php

namespace MyApp\Domain\Exception;

class InvalidPasswordException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Security violation. The password is incorrect!');
    }
}