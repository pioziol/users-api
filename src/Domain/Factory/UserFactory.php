<?php

namespace MyApp\Domain\Factory;

use MyApp\Domain\Entity\User;
use MyApp\Domain\Mapper\DomainFields;
use MyApp\Domain\Service\HashPassword;

class UserFactory
{
    /** @var HashPassword */
    private $hashPassword;

    public function __construct(
        HashPassword $hashPassword
    )
    {
        $this->hashPassword = $hashPassword;
    }

    public function create(array $data):User
    {
        $user = new User();
        $user->setId($data[DomainFields::USER_ID_FIELD]??'');
        $user->setEmail($data[DomainFields::EMAIL_FIELD]??'');
        $password = isset($data[DomainFields::PASSWORD_FIELD]) ? $this->hashPassword->execute($data[DomainFields::PASSWORD_FIELD]) : '';
        $user->setPassword($password);
        $user->setName($data[DomainFields::NAME_FIELD??'']);
        $user->setLastName($data[DomainFields::LAST_NAME_FIELD??'']);
        return $user;
    }

}