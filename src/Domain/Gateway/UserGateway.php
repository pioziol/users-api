<?php

namespace MyApp\Domain\Gateway;

use MyApp\Domain\Entity\User;

interface UserGateway
{
    public function save(User $user);

    public function find(int $id):User;

    public function getPasswordForId(int $id): string;

    public function update(User $user);

    public function findManyWithFilters(array $data, $offset, $limit, $orderBy);

    public function delete(int $id);
}