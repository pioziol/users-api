<?php

namespace MyApp\Domain\Mapper;

class DomainFields
{
    const EMAIL_FIELD = 'email';
    const PASSWORD_FIELD = 'password';
    const NAME_FIELD = 'first_name';
    const LAST_NAME_FIELD = 'last_name';
    const REPEATED_PASSWORD_FIELD = 'repeatedPassword';
    const USER_ID_FIELD = 'id';

}