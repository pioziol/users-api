<?php

namespace MyApp\Domain\Repository;

use MyApp\Domain\Entity\User;
use MyApp\Domain\Gateway\UserGateway;
use MyApp\Domain\Mapper\DomainFields;

class UserRepository
{
    /** @var UserGateway */
    private $userGateway;

    public function __construct(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    public function update(User $user)
    {
        $existingUser = $this->userGateway->find($user->getId());
        $existingUser->setName(
            !empty($user->getName()) ? $user->getName() : $existingUser->getName()
        );
        $existingUser->setLastName(
            !empty($user->getLastName()) ? $user->getLastName() : $existingUser->getLastName()
        );
         $this->userGateway->update($existingUser);
        return $existingUser;
    }

    public function findManyWithFilters(array $data)
    {
        $data = array_filter($data);
        $offset = $data['offset']??0;
        unset($data['offset']);
        $orderBy = $data['orderBy']??DomainFields::USER_ID_FIELD;
        unset($data['orderBy']);
        $limit = $data['limit']??10;
        $limit = $limit > 100 ? 10 : $limit;
        unset($data['limit']);
        return $this->userGateway->findManyWithFilters($data, $offset, $limit, $orderBy);
    }

}