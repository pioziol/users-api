<?php

namespace MyApp\Domain\Service;

class HashPassword
{
    public function execute(string $password) {
        return hash('sha512', $password);
    }

}