<?php

namespace MyApp\Domain\ValueObject;

use MyApp\Domain\Exception\ValidationException;

class EmailVO
{
    private $value;

    public function __construct($email)
    {
        $this->value = $email;
        $this->validate();
    }

    private function validate()
    {
        if(empty($this->value)) throw new ValidationException('Email cannot be empty');
        if(!preg_match('/^.{1,}@.{1,}\..{2,3}$/', $this->value)) throw new ValidationException('Email is incorrect');
    }

    public function getValue()
    {
        return $this->value;
    }
}