<?php

namespace MyApp\Domain\ValueObject;

use MyApp\Domain\Exception\ValidationException;

class PasswordVO
{
    private $value;

    const MIN_PASSWORD_LENGTH = 8;

    public function __construct(string $password)
    {
        $this->value = $password;
        $this->validate();
    }

    public function validate()
    {
        if (self::MIN_PASSWORD_LENGTH > strlen($this->value)) throw new ValidationException('Password is too short');
    }

    public function getValue():string
    {
        return $this->value;
    }

    public function sameAs(PasswordVO $otherPassword):bool
    {
        if(!($this->value === $otherPassword->getValue())) throw new ValidationException('Passwords are not equal');
        return true;
    }
}