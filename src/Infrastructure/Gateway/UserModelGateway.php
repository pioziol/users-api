<?php

namespace MyApp\Infrastructure\Gateway;

use Illuminate\Support\Facades\DB;
use MyApp\Domain\Entity\User;
use MyApp\Domain\Factory\UserFactory;
use MyApp\Domain\Gateway\UserGateway;
use App\User as UserModel;
use MyApp\Infrastructure\Exception\DatabaseOperationException;
use MyApp\Infrastructure\Hydrator\UserModelUserHydrator;

class UserModelGateway implements UserGateway
{
    /** @var UserModelUserHydrator */
    private $hydrator;
    /** @var UserFactory */
    private $userFactory;

    public function __construct(
        UserModelUserHydrator $hydrator,
        UserFactory $userFactory
    )
    {
        $this->hydrator = $hydrator;
        $this->userFactory = $userFactory;
    }

    public function save(User $user)
    {
        $userModel = new UserModel();
        $this->hydrator->hydrate($userModel, $user);
        if (!$userModel->save())
            throw new DatabaseOperationException('Saving user data fail');
        $user->setId($userModel->getId());
    }

    public function find(int $id): User
    {
        $userModel = UserModel::where('id', $id)->first();
        $user = new User();
        $this->hydrator->extract($userModel, $user);
        return $user;
    }

    public function getPasswordForId(int $id): string
    {
        return (UserModel::where('id', $id)->first(['password']))
            ->getPassword();
    }

    public function update(User $user)
    {
        $userModel = UserModel::where('id', $user->getId())->first();
        if (!$userModel->update([
            'last_name' => $user->getLastName(),
            'first_name' => $user->getName()
        ])
        ) throw new DatabaseOperationException('Updating user data failed!');
    }

    public function findManyWithFilters(array $data, $offset, $limit, $orderBy)
    {
        $where = [];
        foreach ($data as $item => $value) {
            $where[] = [$item, 'like', "%$value%"];
        }
        $usersCollection = DB::table('users')
            ->where($where)
            ->offset($offset)
            ->limit($limit)
            ->orderBy($orderBy)
            ->select('id', 'last_name', 'first_name', 'email')
            ->get();
        foreach ($usersCollection as $key => $user) $usersCollection[$key] = $this->userFactory->create(json_decode(json_encode($user), true));
        return $usersCollection;
    }

    public function delete(int $id)
    {
        if (!UserModel::where(UserModelUserHydrator::ID_FIELD, $id)->exists()) throw new DatabaseOperationException('User does not exist!');
        if(UserModel::destroy($id)) return true;
        throw new DatabaseOperationException('Deleting user from database error!');
    }


}