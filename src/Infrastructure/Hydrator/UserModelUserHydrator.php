<?php

namespace MyApp\Infrastructure\Hydrator;


use App\User as UserModel;
use MyApp\Domain\Entity\User;

class UserModelUserHydrator
{

    const ID_FIELD = 'id';
    const EMAIL_FIELD = 'email';
    const PASSWORD_FIELD = 'password';
    const NAME = 'first_name';
    const LAST_NAME = 'last_name';

    public function hydrate(UserModel $userModel, User $user)
    {
        !empty($user->getId()) ? $userModel->setAttribute(self::ID_FIELD, $user->getId()): '';
        $userModel->setAttribute(self::EMAIL_FIELD, $user->getEmail());
        $userModel->setAttribute(self::PASSWORD_FIELD, $user->getPassword());
        $userModel->setAttribute(self::NAME, $user->getName());
        $userModel->setAttribute(self::LAST_NAME, $user->getLastName());
    }

    public function extract(UserModel $userModel, User $user) {
        $user->setId($userModel->getId());
        $user->setEmail($userModel->getEmail());
        $user->setName($userModel->getName());
        $user->setLastName($userModel->getLastName());
    }

}